using _07_LoginApi.Models;
using _07_LoginApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.SignalR;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Claims;

namespace _07_LoginApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllersWithViews();
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSingleton<IMovieService, MovieService>();
            builder.Services.AddSingleton<IUserService, UserService>();
            
            builder.Services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Description = "Bearer Authentication with JWT Token",
                    Type = SecuritySchemeType.Http
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            }
                        },
                        new List<string>()
                    }
                });

            });

            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                {
                    ValidateActor = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = builder.Configuration["Jwt:Issuer"],
                    ValidAudience = builder.Configuration["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"]))
                };
            });
            
            var app = builder.Build();
            app.UseSwagger();
            app.UseAuthorization();
            app.UseAuthentication();

            app.MapPost("/login",
                (UserLogin user, IUserService service) => Login(user,service))
                .Accepts<UserLogin>("application/json")
                .Produces<string>();


            app.MapPost("/create",
                [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Administrator")]
            (Movie movie, IMovieService service) => Create(movie, service))
                .Accepts<Movie>("application/json")
                .Produces<Movie>(statusCode: 200, contentType: "application/json");

            app.MapGet("/get",
                [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Standard, Administrator")]
            (int id, IMovieService service) => Get(id, service))
                .Produces<Movie>();

            app.MapGet("/list",
                (IMovieService service) => List(service))
                .Produces<List<Movie>>(statusCode: 200, contentType: "application/json");

            app.MapPut("/update",
                [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Administrator")]
            (Movie newMovie, IMovieService service) => Update(newMovie, service))
                .Accepts<Movie>("application/json")
                .Produces<Movie>(statusCode: 200, contentType: "application/json");

            app.MapDelete("/delete",
                [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Administrator")]
            (int id, IMovieService service) => Delete(id, service));

            IResult Login(UserLogin user, IUserService service)
            {
                if (!string.IsNullOrEmpty(user.Username) &&
                    !string.IsNullOrEmpty(user.Password))
                {
                    var loggedInUser = service.Get(user);
                    if (loggedInUser is null) return Results.NotFound("User not found");

                    var claims = new[]
                    {
                        new Claim(ClaimTypes.NameIdentifier, loggedInUser.Username),
                        new Claim(ClaimTypes.Email, loggedInUser.EmailAddress),
                        new Claim(ClaimTypes.GivenName, loggedInUser.GivenName),
                        new Claim(ClaimTypes.Surname, loggedInUser.Surname),
                        new Claim(ClaimTypes.Role, loggedInUser.Role),
                    };

                    var token = new JwtSecurityToken
                        (
                            issuer: builder.Configuration["Jwt:Issuer"],
                            audience: builder.Configuration["Jwt:Audience"],
                            claims: claims,
                            expires:DateTime.UtcNow.AddDays(60),
                            notBefore: DateTime.UtcNow,
                            signingCredentials: new SigningCredentials(
                                new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"])),
                                SecurityAlgorithms.HmacSha256)
                        );

                    var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

                    return Results.Ok(tokenString);
                } else
                {
                    return Results.NotFound("User not found");
                }

            }

            IResult Create(Movie movie, IMovieService service)
            {
                var result = service.Create(movie);
                return Results.Ok(result);
            }

            IResult Get(int id, IMovieService service)
            {
                var movie = service.Get(id);
                if (movie is null) return Results.NotFound("Movie not found");
                return Results.Ok(movie);
            }

            IResult List(IMovieService service)
            {
                var movies = service.List();
                return Results.Ok(movies);
            }

            IResult Update(Movie newMovie, IMovieService service)
            {
                var updatedMovie = service.Update(newMovie);
                if (updatedMovie is null) Results.NotFound("Movie not found");
                return Results.Ok(updatedMovie);
            }

            IResult Delete(int id, IMovieService service)
            {
                var result = service.Delete(id);
                if (!result) Results.BadRequest("Something went wrong");
                return Results.Ok(result);
            }

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.Run();
        }
    }
}