﻿using _07_LoginApi.Models;

namespace _07_LoginApi.Services
{
    public interface IUserService
    {
        public User Get(UserLogin userLogin);
    }
}
